const Sequelize = require('sequelize');
const db = require('../config/database');

const Device = db.define('device',{
    id:{
        type: Sequelize.STRING
    },
    devicename:{
        type: Sequelize.STRING
    }
});

module.exports = Device;