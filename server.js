require('dotenv').config()
const express = require('express');
const app = express();
const db = require("./config/database")
const HOST = process.env.SERVER_HOST;
const PORT = process.env.SERVER_PORT;

app.use('/api/device',require('./routes/device_routes'));
app.use('/api/user',require('./routes/user_routes'))

app.listen(HOST, PORT);
console.log(`Running on http://${HOST}:${PORT}`);