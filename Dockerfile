FROM node:10
WORKDIR /usr/src/app
RUN apt-get update && apt-get install -y fish 
COPY package.json /usr/src/app
RUN npm install -g
COPY . .
RUN npm install -g nodemon
EXPOSE 8080/tcp
CMD ["npm","start"]