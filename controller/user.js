const v1_controllerRouter = require('express').Router();

v1_controllerRouter.get("/", (req,res) => {
    res.send("main route");
});


v1_controllerRouter.get("/test", (req,res) => {
    res.send("test api");
});

module.exports.v1controllerRouter = v1_controllerRouter;
