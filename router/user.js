const bcrypt = require('bcrypt');

bcrypt.hash('myPassword', 10, function(err, hash) {
    // Store hash in database
});

bcrypt.compare('somePassword', hash, function(err, res) {
    if(res) {
     // Passwords match
    } else {
     // Passwords don't match
    } 
});