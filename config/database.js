const Sequelize = require('sequelize');

// Option 1: Passing parameters separately
module.exports = new Sequelize(process.env.DB_NAME, process.env.DB_USERNAME, process.env.DB_PASSWORD, {
  host: process.env.HOST,
  dialect:  'postgres',
  pool:{
      max:5,
      min:0,
      acquire:30000,
      idle:10000
  },
});
